INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2023_05_03_000000_create_users_table', 1),
(2, '2023_05_03_100000_create_password_resets_table', 1),
(3, '2023_05_03_000000_create_failed_jobs_table', 1),
(4, '2023_05_03_000001_create_personal_access_tokens_table', 1),
(5, '2023_06_20_111430_add_two_factor_columns_to_table', 1),
(6, '2023_06_20_113007_create_permission_tables', 1),
(7, '2023_06_20_124027_create_project_statuses_table', 1),
(8, '2023_06_20_124028_create_projects_table', 1),
(9, '2023_06_20_131753_create_project_users_table', 1),
(10, '2023_06_20_134510_create_media_table', 1),
(11, '2023_06_20_152359_create_project_favorites_table', 1),
(12, '2023_06_20_193241_create_ticket_statuses_table', 1),
(13, '2023_06_20_193242_create_tickets_table', 1),
(14, '2023_06_25_155109_add_tickets_prefix_to_projects', 1),
(15, '2023_06_25_163226_add_code_to_tickets', 1),
(16, '2023_06_25_164004_create_ticket_types_table', 1),
(17, '2023_06_25_165400_add_type_to_ticket', 1),
(18, '2023_06_25_173220_add_order_to_tickets', 1),
(19, '2023_06_25_184448_add_order_to_ticket_statuses', 1),
(20, '2023_06_25_193051_create_ticket_activities_table', 1),
(21, '2023_06_25_194000_create_ticket_priorities_table', 1),
(22, '2023_06_25_194728_add_priority_to_tickets', 1),
(23, '2023_06_25_203702_add_status_type_to_project', 1),
(24, '2023_06_25_204227_add_project_to_ticket_statuses', 1),
(25, '2023_06_27_064347_create_ticket_comments_table', 1),
(26, '2023_07_03_084509_create_ticket_subscribers_table', 1),
(27, '2023_07_03_144611_create_notifications_table', 1),
(28, '2023_07_03_150309_create_jobs_table', 1),
(29, '2023_07_03_163244_create_ticket_relations_table', 1),
(30, '2023_07_03_172846_create_settings_table', 1),
(31, '2023_07_05_173004_general_settings', 1),
(32, '2023_07_05_173852_create_general_settings', 1),
(33, '2023_07_05_085506_create_socialite_users_table', 1),
(34, '2023_07_05_085638_make_user_password_nullable', 1),
(35, '2023_07_05_110740_remove_unique_from_users', 1),
(36, '2023_07_10_110955_add_soft_deletes_to_users', 1),
(37, '2023_07_13_193214_create_ticket_hours_table', 1),
(38, '2023_07_13_200608_add_estimation_to_tickets', 1),
(39, '2023_07_17_134201_add_creation_token_to_users', 1),
(40, '2023_07_17_142644_create_pending_user_emails_table', 1),

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1);

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'List permissions', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(2, 'View permission', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(3, 'Create permission', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(4, 'Update permission', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(5, 'Delete permission', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(6, 'List projects', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(7, 'View project', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(8, 'Create project', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(9, 'Update project', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(10, 'Delete project', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(11, 'List project statuses', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(12, 'View project status', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(13, 'Create project status', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(14, 'Update project status', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(15, 'Delete project status', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(16, 'List roles', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(17, 'View role', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(18, 'Create role', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(19, 'Update role', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(20, 'Delete role', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(21, 'List tickets', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(22, 'View ticket', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(23, 'Create ticket', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(24, 'Update ticket', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(25, 'Delete ticket', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(26, 'List ticket priorities', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(27, 'View ticket priority', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(28, 'Create ticket priority', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(29, 'Update ticket priority', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(30, 'Delete ticket priority', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(31, 'List ticket statuses', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(32, 'View ticket status', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(33, 'Create ticket status', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(34, 'Update ticket status', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(35, 'Delete ticket status', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(36, 'List ticket types', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(37, 'View ticket type', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(38, 'Create ticket type', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(39, 'Update ticket type', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(40, 'Delete ticket type', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(41, 'List users', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(42, 'View user', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(43, 'Create user', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(44, 'Update user', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(45, 'Delete user', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56'),
(46, 'Manage general settings', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56');

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Default role', 'web', '2023-08-09 12:06:56', '2023-08-09 12:06:56');

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1);

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `two_factor_confirmed_at`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `creation_token`) VALUES
(1, 'John DOE', 'admin@edge.app', '2023-08-09 12:06:56', '$2a$12$h/.Jq3QGHYoJBLBo8hw1mOtJOmtU.BVJFbBWFC7XAVXmE5gOjdXV.', NULL, NULL, NULL, NULL, '2023-08-09 12:06:56', '2023-08-09 12:06:56', NULL, '16a26769-e643-4570-b3d6-9b9604fe49bc');

INSERT INTO `settings` (`id`, `group`, `name`, `locked`, `payload`, `created_at`, `updated_at`) VALUES
(1, 'general', 'site_name', 0, '\"EdgeApp\"', '2023-08-09 18:11:50', '2023-08-09 18:11:50'),
(2, 'general', 'site_logo', 0, 'null', '2023-08-09 18:11:50', '2023-08-09 18:11:50'),
(3, 'general', 'enable_registration', 0, 'true', '2023-08-09 18:11:50', '2023-08-09 18:11:50'),
(4, 'general', 'enable_social_login', 0, 'true', '2023-08-09 18:11:50', '2023-08-09 18:11:50'),
(5, 'general', 'site_language', 0, 'en', '2023-08-09 18:11:50', '2023-08-09 18:11:50');

ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

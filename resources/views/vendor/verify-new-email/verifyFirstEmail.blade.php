@component('mail::message')
# Verifier l'addresse email

Veuillez cliquer sur le bouton ci-dessous pour vérifier votre adresse électronique.

@component('mail::button', ['url' => $url])
Verifier l'adresse mail
@endcomponent

Si vous n'avez pas créé de compte, aucune autre action n'est requise.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
@component('mail::message')
# Vérifier la nouvelle adresse mail

Veuillez cliquer sur le bouton ci-dessous pour vérifier votre nouvelle adresse électronique.

@component('mail::button', ['url' => $url])
Vérifier la nouvelle adresse mail
@endcomponent

Si vous n'avez pas mis à jour votre adresse électronique, aucune autre action n'est requise.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
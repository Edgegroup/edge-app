<x-filament::layouts.card>

    <form wire:submit.prevent="validateAccount">

        <div class="mb-10">
            <h2 class="font-bold tracking-tight text-center text-2xl">
                {{ __('Vérifier mon compte') }}
            </h2>
            <p class="mt-2 text-sm text-center">
                {{ __('Utilisez le formulaire suivant pour choisir le mot de passe de votre compte et valider la création de votre compte') }}
            </p>
        </div>

        {{ $this->form }}

        <x-filament::button type="submit" class="w-full mt-5">
            {{ __('Vérifier') }}
        </x-filament::button>

    </form>


</x-filament::layouts.card>

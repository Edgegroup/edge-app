<?php

namespace App\Filament\Pages;

use App\Filament\Widgets\Lastprojects;
use App\Filament\Widgets\Lasttickets;
use Filament\Pages\Dashboard as BasePage;

class Dashboard extends BasePage
{
    protected static bool $shouldRegisterNavigation = false;

    protected function getColumns(): int | array
    {
        return 6;
    }

    protected function getWidgets(): array
    {
        return [
            Lastprojects::class,
            Lasttickets::class,
        ];
    }
}

<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TicketResource\Pages;
use App\Models\Project;
use App\Models\Ticket;
use App\Models\TicketPriority;
use App\Models\TicketStatus;
use App\Models\TicketType;
use App\Models\User;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Support\HtmlString;

class TicketResource extends Resource
{
    protected static ?string $model = Ticket::class;

    protected static ?string $navigationIcon = 'heroicon-o-ticket';

    protected static ?int $navigationSort = 2;

    protected static function getNavigationLabel(): string
    {
        return __('Tickets');
    }

    public static function getPluralLabel(): ?string
    {
        return static::getNavigationLabel();
    }

    protected static function getNavigationGroup(): ?string
    {
        return __('Gestion');
    }

    public static function table(Table $table): Table
    {
       
        $colonnes = [
            Tables\Columns\TextColumn::make('project.name')
                ->label(__('Projet'))
                ->sortable()
                ->searchable(),

            Tables\Columns\TextColumn::make('name')
                ->label(__('Nom du ticket'))
                ->sortable()
                ->searchable(),
    
            Tables\Columns\TextColumn::make('owner.name')
                ->label(__('Propriétaire'))
                ->sortable()
                ->formatStateUsing(fn($record) => view('components.user-avatar', ['user' => $record->owner]))
                ->searchable(),
    
            Tables\Columns\TextColumn::make('responsible.name')
                ->label(__('Responsable'))
                ->sortable()
                ->formatStateUsing(fn($record) => view('components.user-avatar', ['user' => $record->responsible]))
                ->searchable(),
    
            Tables\Columns\TextColumn::make('status.name')
                ->label(__('Statut'))
                ->formatStateUsing(fn($record) => new HtmlString('
                            <div class="flex items-center gap-2 mt-1">
                                <span class="filament-tables-color-column relative flex h-6 w-6 rounded-md"
                                    style="background-color: ' . $record->status->color . '"></span>
                                <span>' . $record->status->name . '</span>
                            </div>
                        '))
                ->sortable()
                ->searchable(),
    
            Tables\Columns\TextColumn::make('type.name')
                ->label(__('Type'))
                ->formatStateUsing(
                    fn($record) => view('partials.filament.resources.ticket-type', ['state' => $record->type])
                )
                ->sortable()
                ->searchable(),
    
            Tables\Columns\TextColumn::make('priority.name')
                ->label(__('Priorité'))
                ->formatStateUsing(fn($record) => new HtmlString('
                            <div class="flex items-center gap-2 mt-1">
                                <span class="filament-tables-color-column relative flex h-6 w-6 rounded-md"
                                    style="background-color: ' . $record->priority->color . '"></span>
                                <span>' . $record->priority->name . '</span>
                            </div>
                        '))
                ->sortable()
                ->searchable(),
    
            Tables\Columns\TextColumn::make('created_at')
                ->label(__('Créé le'))
                ->dateTime()
                ->sortable()
                ->searchable(),
        ];
    
       
        $filters = [
            Tables\Filters\SelectFilter::make('project_id') //Créer les filtres
                ->label(__('Projet'))
                ->multiple()
                ->options(fn() => Project::where('owner_id', auth()->user()->id)
                    ->orWhereHas('users', function ($query) {
                        return $query->where('users.id', auth()->user()->id);
                    })->pluck('name', 'id')->toArray()),
    
            Tables\Filters\SelectFilter::make('owner_id')
                ->label(__('Propriétaire'))
                ->multiple()
                ->options(fn() => User::all()->pluck('name', 'id')->toArray()),
    
            Tables\Filters\SelectFilter::make('responsible_id')
                ->label(__('Responsable'))
                ->multiple()
                ->options(fn() => User::all()->pluck('name', 'id')->toArray()),
    
            Tables\Filters\SelectFilter::make('status_id')
                ->label(__('Statut'))
                ->multiple()
                ->options(fn() => TicketStatus::all()->pluck('name', 'id')->toArray()),
    
            Tables\Filters\SelectFilter::make('type_id')
                ->label(__('Type'))
                ->multiple()
                ->options(fn() => TicketType::all()->pluck('name', 'id')->toArray()),
    
            Tables\Filters\SelectFilter::make('priority_id')
                ->label(__('Priorité'))
                ->multiple()
                ->options(fn() => TicketPriority::all()->pluck('name', 'id')->toArray()),
        ];
    
        
        return $table   //Creer la table
            ->columns($colonnes)
            ->filters($filters)
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Card::make()
                    ->schema([
                        Forms\Components\Grid::make()
                            ->schema([
                                Forms\Components\Grid::make()
                                    ->columns(2)
                                    ->columnSpan(3)
                                    ->schema([
                                        Forms\Components\TextInput::make('name') // Element de nom
                                            ->label(__('Nom du ticket'))
                                            ->required()
                                            ->columnSpan(3)
                                            ->maxLength(255),
                                Forms\Components\Select::make('project_id') // Element Id du projet
                                    ->label(__('Projet'))
                                    ->searchable()
                                    ->reactive()
                                    ->afterStateUpdated(function ($get, $set) {
                                        $project = Project::where('id', $get('project_id'))->first();          
                                        $set(
                                            'status_id',
                                            TicketStatus::whereNull('project_id')
                                                ->where('is_default', true)
                                                ->first()
                                                ?->id
                                            );
                                       
                                    })
                                    ->options(fn() => Project::where('owner_id', auth()->user()->id)
                                        ->orWhereHas('users', function ($query) {
                                            return $query->where('users.id', auth()->user()->id);
                                        })->pluck('name', 'id')
                                        ->toArray()
                                    )
                                    ->default(fn() => request()->get('project'))
                                    ->required(),
                                
                                    ]),

                                Forms\Components\Select::make('owner_id') // Element Propriétaire Ticket
                                    ->label(__('Propriétaire du ticket'))
                                    ->searchable()
                                    ->options(fn() => User::all()->pluck('name', 'id')->toArray())
                                    ->default(fn() => auth()->user()->id)
                                    ->required(),

                                Forms\Components\Select::make('responsible_id') // Element Responsable
                                    ->label(__('Responsable du ticket'))
                                    ->searchable()
                                    ->options(fn() => User::all()->pluck('name', 'id')->toArray()),

                                Forms\Components\Grid::make() // Grille des états
                                    ->columns(3)
                                    ->columnSpan(2)
                                    ->schema([
                                        Forms\Components\Select::make('status_id') // Element Statut
                                            ->label(__('Statut du ticket'))
                                            ->searchable()
                                            ->options(function ($get) {
                                                $project = Project::where('id', $get('project_id'))->first();
                                                return TicketStatus::whereNull('project_id')
                                                    ->get()
                                                    ->pluck('name', 'id')
                                                    ->toArray();
                                                
                                            })
                                            ->default(function ($get) {
                                                $project = Project::where('id', $get('project_id'))->first();
                                                return TicketStatus::whereNull('project_id')
                                                    ->where('is_default', true)
                                                    ->first()
                                                    ?->id;
                                                
                                                 })
                                            ->required(),

                                        Forms\Components\Select::make('type_id') // Element type
                                            ->label(__('Type du ticket'))
                                            ->searchable()
                                            ->options(fn() => TicketType::all()->pluck('name', 'id')->toArray())
                                            ->default(fn() => TicketType::where('is_default', true)->first()?->id)
                                            ->required(),

                                        Forms\Components\Select::make('priority_id') // element priorité
                                            ->label(__('Priorité du ticket'))
                                            ->searchable()
                                            ->options(fn() => TicketPriority::all()->pluck('name', 'id')->toArray())
                                            ->default(fn() => TicketPriority::where('is_default', true)->first()?->id)
                                            ->required(),
                                    ]),
                            ]),

                        Forms\Components\RichEditor::make('content')
                            ->label(__('Contenu du ticket'))
                            ->required()
                            ->columnSpan(2),

                        Forms\Components\Grid::make()
                            ->columnSpan(2)
                            ->columns(8)
                            ->schema([
                                Forms\Components\TextInput::make('estimation')
                                    ->label(__('Durée estimée'))
                                    ->numeric()
                                    ->columnSpan(2),
                            ]),
                    ]),
            ]);
    }



    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTickets::route('/'),
            'create' => Pages\CreateTicket::route('/create'),
            'view' => Pages\ViewTicket::route('/{record}'),
            'edit' => Pages\EditTicket::route('/{record}/edit'),
        ];
    }
}

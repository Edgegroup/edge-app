<?php

namespace App\Filament\Resources\ProjetRessource\Pages;

use App\Filament\Resources\ProjetRessource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateProject extends CreateRecord
{
    protected static string $resource = ProjetRessource::class;
}

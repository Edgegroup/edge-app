<?php

namespace App\Filament\Resources\ProjetRessource\Pages;

use App\Filament\Resources\ProjetRessource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ViewRecord;

class ViewProject extends ViewRecord
{
    protected static string $resource = ProjetRessource::class;

    protected function getActions(): array
    {
        return [
            Actions\EditAction::make(),
        ];
    }
}
